<?php
echo "Test Script Starting\n";
require('functions.inc.php');

$x=25;
$expect=5;

$answer=squareRoot($x);

echo "Test Result: √".$x."=".$answer." (expected: ".$expect.")\n";

if ($answer==$expect)
{
    echo "Test Passed\n";
    exit(0); // exit code 0 - success
}
else
{
    echo "Test Failed\n";
    exit(1); // exit code not zero - error
}
