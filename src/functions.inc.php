<?php
class SquareRootOfNegativeException extends Exception {};
class SquareRootOfZeroException extends Exception {};
class NonIntegerException extends Exception {};

function squareRoot($x)
{
    
    try
    {
        if ($x == 0)
        {
            throw new SquareRootOfZeroException();
        }
        else if ($x < 0)
        {
            throw new SquareRootOfNegativeException();
        }
        else if ($x !== is_int) 
        {
            throw new NonIntegerException();
        }
    }
    catch (MultiplyByZeroException $ex)
    {
        echo "Multiply by zero exception!";
    }
    catch (DivideByNegativeException $ex)
    {
        echo "Multiply by negative number exception!";
    }
    catch (NonIntegerException $ex)
    {
        echo "You can only multiply using Integers!";
    }
    catch (Exception $x)
    {
        echo "UNKNOWN EXCEPTION!";
    }
    
        $n = $x;
        $y = 1;
        while($n > $y)
        {
            $n = ($n + $y)/2;
            $y = $x/$n;
        }
        
        $answer = round($n);
        return $answer;
}

